﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CameraMovement : MonoBehaviour
{
    public Transform target;

    private Vector3 offssetPos;

    public float smoothPos;

    public void Start()
    {
        offssetPos = transform.position - target.position;
    }

    private void FixedUpdate()
    {
        transform.position = Vector3.Lerp(transform.position, target.position + offssetPos, Time.fixedDeltaTime * smoothPos);
    }
}
