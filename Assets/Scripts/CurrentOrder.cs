﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CurrentOrder : MonoBehaviour
{
    public GameObject orderTargetRestaurant;
    public GameObject orderTargetClient;

    public bool orderBoolRestaurant;
    public bool orderBoolClient;

    public GameObject deliveryZone;

    private bool activeDeliveryZoneRestaurant = false;
    private bool activeDeliveryZoneClient = false;

    public void Update()
    {
        if (orderTargetRestaurant != null)
        {
            if (!orderBoolRestaurant && !activeDeliveryZoneRestaurant)
            {
                SpawnDeliveryZone(orderTargetRestaurant.transform);
                activeDeliveryZoneRestaurant = true;
            }
        }

        if (orderTargetClient != null)
        {
            if (orderBoolRestaurant && !orderBoolClient && !activeDeliveryZoneClient)
            {
                SpawnDeliveryZone(orderTargetClient.transform);
                activeDeliveryZoneClient = true;
            }
        }
    }

    public void OnTriggerEnter(Collider other)
    {
        if (other.gameObject.tag == "DeliveryZone" && !orderBoolRestaurant)
        {
            orderBoolRestaurant = true;
            Destroy(other.gameObject);
        }
        else if (other.gameObject.tag == "DeliveryZone" && orderBoolRestaurant)
        {
            orderBoolClient = true;
            Destroy(other.gameObject);
        }
    }

    public void SpawnDeliveryZone(Transform posReference)
    {
        if (!orderBoolRestaurant)
        {

        }

        if (orderBoolRestaurant && !orderBoolClient)
        {
            Vector3 posClient = new Vector3(64, 0, 64);
            Instantiate(deliveryZone, posClient, transform.rotation);
        }
    }
}
