﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LevelGeneration : MonoBehaviour
{
    public Vector2 levelUnit;

    public GameObject centralBuilding;
    public GameObject roadObject;
    public GameObject[] buildingObject;
    public GameObject[] restaurantObject;

    public List<GameObject> roadsList = new List<GameObject>();

    public void Start()
    {
        GenerateLevel();
    }

    public void GenerateLevel()
    {
        Instantiate(centralBuilding, Vector3.zero, transform.rotation);

        //Generation X axis
        for (int x = 0; x < levelUnit.x; x++)
        {
            if (roadsList.Count == 0)
            {
                //Positif x axis
                GameObject roadX = Instantiate(roadObject, new Vector3(transform.position.x + 10, 0, 0), transform.rotation);
                roadsList.Add(roadX);

                //Négative x axis
                GameObject roadx = Instantiate(roadObject, new Vector3(transform.position.x - 10, 0, 0), transform.rotation);
                roadsList.Add(roadx);
            }
            else
            {
                //Positif x axis
                GameObject roadX = Instantiate(roadObject, new Vector3(transform.position.x + roadsList[x - 1].transform.position.x + 10, 0, 0), transform.rotation);
                roadsList.Add(roadX);

                //Négative x axis
                GameObject roadx = Instantiate(roadObject, new Vector3(transform.position.x - roadsList[x - 1].transform.position.x - 10, 0, 0), transform.rotation);
                roadsList.Add(roadx);
            }
        }
    }
}
