﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerCharacter : MonoBehaviour
{
    [Header("Global variables :")]
    public float speed;
    private float iniSpeed;
    public float speedTurn;
    private float iniSpeedTurn;

    [Header("Slots object")]
    public Transform originLauchObject;
    public GameObject objectSlot1;
    public GameObject objectSlot2;

    private Vector3 moveForward = Vector3.zero;

    private Rigidbody rb;

    public bool drift;
    public float driftDirection;

    private void Start()
    {
        rb = GetComponent<Rigidbody>();

        iniSpeed = speed;
        iniSpeedTurn = speedTurn;

        drift = false;
    }

    public void FixedUpdate()
    {
        if (Input.GetAxis("Vertical") > 0 || Input.GetKey(KeyCode.Joystick1Button0))
        {
            rb.MovePosition(transform.localPosition + transform.right * Time.fixedDeltaTime * speed);
        }
        else if (Input.GetAxis("Vertical") < 0 || Input.GetKey(KeyCode.Joystick1Button1))
        {
            rb.MovePosition(transform.localPosition - transform.right * Time.fixedDeltaTime * (speed / 2));
        }

        if (Input.GetButton("Vertical") || Input.GetKey(KeyCode.Joystick1Button0) || Input.GetKey(KeyCode.Joystick1Button1))
        {
            Quaternion deltaRotation = Quaternion.Euler(new Vector3(0, Input.GetAxis("Horizontal"), 0) * Time.deltaTime * speedTurn);
            rb.MoveRotation(rb.rotation * deltaRotation);
        }
    }

    public void Update()
    {
        //Drift
        if (Input.GetKeyDown(KeyCode.Space) && !drift && Input.GetAxis("Horizontal") != 0)
        {
            drift = true;
            driftDirection = Input.GetAxis("Horizontal") > 0 ? 1 : -1;
        }

        if (drift)
        {

        }

        if (Input.GetKeyUp(KeyCode.Space) && drift)
        {
            drift = false;
        }

        //Launch slot 1
        if (Input.GetKey(KeyCode.A))
        {
            LaunchObjectSlot1();
        }

        //Launch slot 2
        if (Input.GetKey(KeyCode.E))
        {
            LaunchObjectSlot2();
        }
    }

    public void LaunchObjectSlot1()
    {
        if (objectSlot1 != null)
        {
            Instantiate(objectSlot1, originLauchObject.position, originLauchObject.rotation);
            objectSlot1 = null;
        }
    }

    public void LaunchObjectSlot2()
    {
        if (objectSlot2 != null)
        {
            Instantiate(objectSlot2, originLauchObject.position, originLauchObject.rotation);
            objectSlot2 = null;
        }
    }
}
