﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RestaurantGeneration : MonoBehaviour
{
    public GameObject[] buildings;

    public int numberRestaurants;

    public GameObject[] restaurants;

    public Material SubwayMat;
    public Material McdoMat;
    public Material KfcMat;
    public Material BurgerKingMat;

    void Start()
    {
        buildings = GameObject.FindGameObjectsWithTag("Building");
    }

    public void GenerateRestaurants()
    {
        for (int i = 0; i < numberRestaurants; i++)
        {
            int rdmNumber = Random.Range(0, buildings.Length);

            if (buildings[rdmNumber].GetComponent<Renderer>().material != SubwayMat)
            {
                if (buildings[rdmNumber].GetComponent<Renderer>().material != McdoMat)
                {
                    if (buildings[rdmNumber].GetComponent<Renderer>().material != KfcMat)
                    {
                        if (buildings[rdmNumber].GetComponent<Renderer>().material != BurgerKingMat)
                        {

                        }
                        else
                        {
                            GenerateRestaurants();
                        }
                    }
                }
            }
        }
    }
}
